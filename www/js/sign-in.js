      $(function() {
        $('#alpha-sign-in').submit(function(e) {
          e.preventDefault();
          localStorage.clear();
          $.post('http://localhost:3000/api/v1/users/sign_in', $(this).serialize()).done(function(response) {
              localStorage.auth_token = response.auth_token
              location.href = 'pet-detail.html'
            });
          console.log(localStorage.auth_token);
        });
      });