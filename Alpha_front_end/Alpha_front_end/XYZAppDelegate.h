//
//  XYZAppDelegate.h
//  Alpha_front_end
//
//  Created by Timur Kalimov on 2/7/14.
//  Copyright (c) 2014 Roundview. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XYZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
