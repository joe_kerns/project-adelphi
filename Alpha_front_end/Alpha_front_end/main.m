//
//  main.m
//  Alpha_front_end
//
//  Created by Timur Kalimov on 2/7/14.
//  Copyright (c) 2014 Roundview. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XYZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XYZAppDelegate class]));
    }
}
